// swift-tools-version:4.1
import PackageDescription

let package = Package(
    name: "wifiSnooper",
    products: [
        .library(name: "wifiSnooper", targets: ["App"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.3.0"),

        // 🔵 Swift ORM (queries, models, relations, etc) built on PostgreSQL.
        .package(url: "https://github.com/vapor/fluent-postgres-driver", from: "1.0.0"),
        
        .package(url: "https://gitlab.com/wifisnoopers/fluent-postgis.git", .branch("master"))
    ],
    targets: [
        .target(name: "App", dependencies: ["FluentPostgreSQL", "Vapor", "FluentPostGIS"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)
