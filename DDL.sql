CREATE EXTENSION postgis;

create table password_type (
    password_type   VARCHAR(5)     NOT NULL PRIMARY KEY
);

CREATE TABLE hotspot (
    id              SERIAL         NOT NULL PRIMARY KEY,
    ssid            VARCHAR(255)   NOT NULL,
    password        VARCHAR(255)   NULL,
    password_type   VARCHAR(5)     NULL REFERENCES password_type(password_type),
    location        geography      NOT NULL
);

CREATE INDEX ON hotspot(location);

INSERT INTO password_type values
    ('WEP'),
    ('WPA'),
    ('WPA2'),
    ('WPA3')