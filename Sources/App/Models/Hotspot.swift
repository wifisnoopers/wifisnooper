//
//  Hotspot.swift
//  App
//
//  Created by Stein Carsten Sinke on 11/09/2019.
//

import Foundation
import Vapor
import FluentPostgreSQL
import FluentPostGIS

struct Hotspot: PostgreSQLModel {
    static let entity = "hotspot"
    
    var id: Int?
    var ssid: String
    var password: String?
    var passwordType: String?
    var location: GeographicPoint2D
    
    enum CodingKeys: String, CodingKey {
        case id, ssid, password, passwordType = "password_type", location
    }
}

extension Hotspot {
    func convertToDTO() -> HotspotDTO {
        return HotspotDTO(id: id, ssid: ssid, password: password, passwordType: passwordType, location: location)
    }
}

extension Hotspot: Content {}

extension Hotspot: Parameter {}
