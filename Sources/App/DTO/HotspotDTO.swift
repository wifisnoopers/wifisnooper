//
//  HotspotDTO.swift
//  App
//
//  Created by Stein Carsten Sinke on 02/10/2019.
//

import Foundation
import FluentPostGIS
import Vapor

struct HotspotDTO: Codable, Content {
    static let entity = "hotspot"
    
    var id: Int?
    var ssid: String
    var password: String?
    var passwordType: String?
    var location: GeographicPoint2D
}
