import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let hotspotController = HotspotController()
    router.get("hotspots", use: hotspotController.getAll)
    router.get("hotspots", Int.parameter, use: hotspotController.getById)
    router.post("hotspots", use: hotspotController.post)
}
