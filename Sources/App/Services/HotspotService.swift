//
//  HotspotService.swift
//  App
//
//  Created by Sven van der Vlist on 12/09/2019.
//

import Foundation
import Vapor

struct HotspotService {
    
    func getAll(_ req: Request) throws -> Future<[Hotspot]> {
        return Hotspot.query(on: req).all()
    }
    
    func getId(_ req: Request, id: Int) -> Future<Hotspot?> {
        return Hotspot.find(id, on: req)
    }
    
    func create(_ req: Request, hotspot: Hotspot) throws -> Future<Hotspot> {
        return hotspot.create(on: req)
    }
}

extension HotspotService : Service {}
