//
//  HotSpotController.swift
//  App
//
//  Created by Stein Carsten Sinke on 11/09/2019.
//

import Foundation
import Vapor

struct HotspotController {
    func getAll(_ req: Request) throws -> Future<[HotspotDTO]> {
        let hotspotService = try req.make(HotspotService.self)
        return try hotspotService.getAll(req).map(to: [HotspotDTO].self) { hotspots in
            var hotspotsDto: [HotspotDTO] = []
            for hotspot in hotspots {
                hotspotsDto.append(hotspot.convertToDTO())
            }
            return hotspotsDto
        }
    }
    
    func post(_ req: Request) throws -> Future<HotspotDTO> {
        let hotspotService = try req.make(HotspotService.self)
        
        let hotspotDTO = try req.content.decode(Hotspot.self)
        
        let hotspot = hotspotDTO.flatMap(to: Hotspot.self) { hotspot in
            return try hotspotService.create(req, hotspot: hotspot)
        }
        
        return hotspot.map(to: HotspotDTO.self) { hotspot in
            return hotspot.convertToDTO()
        }
    }
    
    func getById(_ req: Request) throws -> Future<HotspotDTO> {
        let id = try req.parameters.next(Int.self)
        
        let hotspotService = try req.make(HotspotService.self)
        
        return hotspotService.getId(req, id: id).thenThrowing() { hotspot in
            guard let hotspot = hotspot else {
                throw Abort(HTTPResponseStatus.notFound)
            }
            return hotspot.convertToDTO()
        }
    }
}

