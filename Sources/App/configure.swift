import FluentPostgreSQL
import FluentPostGIS
import Vapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    // Register providers first
    try services.register(FluentPostgreSQLProvider())
    try services.register(FluentPostGISProvider())
    services.register(HotspotService())

    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    let corsConfiguration = CORSMiddleware.Configuration(allowedOrigin: .all, allowedMethods: [.GET, .POST, .PUT, .DELETE, .PATCH, .OPTIONS], allowedHeaders: [.accept, .authorization, .contentType, .origin, .xRequestedWith, .userAgent, .accessControlAllowOrigin])
    let corsMiddleware = CORSMiddleware(configuration: corsConfiguration)
    middlewares.use(corsMiddleware)
    // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    // Configure a SQLite database
    guard let hostname = Environment.get("DATABASE_HOSTNAME") else {
        fatalError("DATABASE_HOSTNAME not available")
    }
    guard let port = Environment.get("DATABASE_PORT") else {
        fatalError("DATABASE_PORT not available")
    }
    guard let username = Environment.get("DATABASE_USER") else {
        fatalError("DATABASE_USER not available")
    }
    guard let password = Environment.get("DATABASE_PASS") else {
        fatalError("DATABASE_PASS not available")
    }
    guard let database = Environment.get("DATABASE_NAME") else {
        fatalError("DATABASE_NAME not available")
    }
    
    let dbConfig = PostgreSQLDatabaseConfig(hostname: hostname, port: Int(port)!, username: username, database: database, password: password, transport: .cleartext)

    let postgreSQL = PostgreSQLDatabase(config: dbConfig)

    // Register the configured SQLite database to the database config.
    var databases = DatabasesConfig()
    databases.add(database: postgreSQL, as: .psql)
    services.register(databases)
    
    Hotspot.defaultDatabase = .psql
}
